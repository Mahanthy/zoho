package com.Zoho.pages.components.common.session;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoBasePage;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.session.contacts.NewContactsPage;
import com.Zoho.pages.session.lead.LeadsPage;
import com.Zoho.pages.session.lead.NewLeadsPage;
import com.Zoho.pages.session.tasks.NewTaskPage;

public class Header {


	@FindBy(xpath=Constant.CONTACTS_HEADER)
	WebElement contactsHeader;
	@FindBy(xpath=Constant.LEADS_HEADER)
	WebElement leadsHeader;
	@FindBy(id=Constant.ADD_ICON)
	WebElement addIcon;
	@FindBy(id=Constant.SUBMENU_LEADS)
	WebElement subMenuLeads;
	@FindBy(id=Constant.SUBMENU_CONTACTS)
	WebElement subMenuContacts;
	@FindBy(id=Constant.SUBMENU_TASK)
	WebElement subMenuTask;

	public Header(WebDriver driver) {
		System.out.println ("**** Header Page Constructor ****");
		PageFactory.initElements(driver, this);
	}
	
	public ZohoPage create(String strElementText) {
		
		addIcon.click();
		if (strElementText.equalsIgnoreCase("Leads")) {
			subMenuLeads.click();
			return new NewLeadsPage();
		}
		else if (strElementText.equalsIgnoreCase("Contacts")) {
			subMenuContacts.click();
			return new NewContactsPage();
		}
		else if (strElementText.equalsIgnoreCase("Task")) {
			subMenuTask.click();
			return new NewTaskPage();
		}
		return new ZohoBasePage();
	}
	
	public void logOut() {
		
	}
	
	public ZohoPage goToLeads() {
		leadsHeader.click();
		return new LeadsPage();
		
	}
}
