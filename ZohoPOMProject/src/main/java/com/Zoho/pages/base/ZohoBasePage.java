package com.Zoho.pages.base;

import java.util.Hashtable;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Reporter;

import com.Zoho.pages.components.common.session.Header;
import com.Zoho.pages.normal.EnterPasswordPage;
import com.Zoho.pages.normal.EnterUserNamePage;
import com.Zoho.pages.session.lead.LeadsDetailsPage;
import com.Zoho.pages.session.lead.LeadsPage;
import com.Zoho.session.ZohoTestSession;
import com.Zoho.web.WebConnector;

public class ZohoBasePage implements ZohoPage{

	public ZohoBasePage() {
		System.out.println("**** ZohoBasPage Constructor");
		PageFactory.initElements(getCurrentDriver(), this);
		getSession().setCurrentPage(this);
	}
	public ZohoPage openBrowser(String browser) {
		// TODO Auto-generated method stub
		return null;
	}

	public void quitBrowser() {
		// TODO Auto-generated method stub
		
	}

	public int getTotalWindows() {
		// TODO Auto-generated method stub
		return 0;
	}

	public ZohoPage navigatetoHomePage() {
		// TODO Auto-generated method stub
		return null;
	}

	public void gotoLoginPage() {
		// TODO Auto-generated method stub
		
	}

	public ZohoPage submitUser(String strUserName) {
		// TODO Auto-generated method stub
		return null;
	}

	public ZohoPage submitPassword(String strPassword) {
		
		return null;
	}
	
	public WebConnector validator(boolean stopExecution) {
		// TODO Auto-generated method stub
		 getSession().getCon().setStopExecution(stopExecution);
		return getSession().getCon();
	}

	public void createDeal() {
		// TODO Auto-generated method stub
		
	}

	public void logout() {
		// TODO Auto-generated method stub
		
	}

	public ZohoTestSession getSession() {
		// TODO Auto-generated method stub
		return (ZohoTestSession) Reporter.getCurrentTestResult().getTestContext().getAttribute("session");
		
	}
	
	public WebConnector getDriver() {
		return getSession().getCon();
	}

	public ZohoPage gotoEnterUserNamePage() {
		return null;
	}
	
	public EventFiringWebDriver getCurrentDriver() {
		return getSession().getCon().getCurrentDriver();
	}
	
	public void log(String strMessage) {
		getSession().log(strMessage);
	}
	
	public void waitForPageToLoad() {
		JavascriptExecutor js = (JavascriptExecutor) getCurrentDriver();
		int i = 1;
		// check for page load max wait is 20 sec
		
		while (i!=10) {
			String state = (String)js.executeScript("return document.readyState;");
			if (state.equals("complete"))
				break;
			else
				wait(2);
			i++;
		}
		// check for jquery /ajax status - max wait 20 sec
		i=1;
		while (i!=10) {
			Long value = (Long)js.executeScript("return jQuery.active;");
			if (value.longValue()==0)
				break;
			else
				wait(2);
			i++;
		}
	}
	
	public void wait(int time) {
		try {
			Thread.sleep(time*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ZohoPage validateIntermediatePageOptions() {
		return null;
	}
	
	public ZohoPage gotoDefaultLandingPage() {
		return null;
	}
	public Header getHeader() {
		return null;
	}
	
	public LeadsDetailsPage submitLeadDetails(String strLeadCompany, String strLeadLastName) {
		return null;
	}
	
	public LeadsPage gotoLeadsPage() {
		return null;
	}
	
	public ZohoPage validateLeadDetails() {
		return null;
	}
	public ZohoPage validateLeadPresenceInList() {
		return null;
	}
	public ZohoPage createTask (Hashtable <String,String> data) {
		return null;
	}
}
