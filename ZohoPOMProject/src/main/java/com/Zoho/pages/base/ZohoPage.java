package com.Zoho.pages.base;

import java.util.Hashtable;

import com.Zoho.pages.components.common.session.Header;
import com.Zoho.pages.normal.EnterPasswordPage;
import com.Zoho.pages.session.lead.LeadsDetailsPage;
import com.Zoho.pages.session.lead.LeadsPage;
import com.Zoho.session.ZohoTestSession;
import com.Zoho.web.WebConnector;

public interface ZohoPage extends ZohoNormalPage, ZohoSessionPage {
	// Normal browser operations
	ZohoPage openBrowser(String browser);
	void quitBrowser();
	int getTotalWindows();
	ZohoTestSession getSession();
	WebConnector getDriver();
	
	// Page Operations
	ZohoPage navigatetoHomePage();	
	ZohoPage gotoEnterUserNamePage(); 
	void gotoLoginPage();
	ZohoPage submitUser(String strUserName);
	ZohoPage submitPassword(String strPassword);
	WebConnector validator(boolean stopExecution);
	public ZohoPage validateIntermediatePageOptions();
	public ZohoPage gotoDefaultLandingPage();
	public void log(String strMessage);
	// Session based Operations
	void createDeal();
	public Header getHeader();
	public LeadsDetailsPage submitLeadDetails(String strLeadCompany, String strLeadLastName);
	public LeadsPage gotoLeadsPage();
	public ZohoPage validateLeadDetails();
	public ZohoPage validateLeadPresenceInList();
	public ZohoPage createTask (Hashtable <String,String> data);
	void logout();
	
}
