package com.Zoho.pages.base;

import com.Zoho.pages.normal.EnterPasswordPage;
import com.Zoho.web.WebConnector;

public interface ZohoNormalPage {

	
	// Page Operations
	ZohoPage navigatetoHomePage();	
	ZohoPage gotoEnterUserNamePage();
	void gotoLoginPage();
	ZohoPage submitUser(String strUserName);
	ZohoPage submitPassword(String strPassword);
	WebConnector validator(boolean stopExecution);
	
}
