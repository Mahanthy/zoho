package com.Zoho.pages.base;

import org.openqa.selenium.By;

public class Constant {

	public static final String DEFAULT_USERNAME = "kiran_nelson@yahoo.com";
	public static final String DEFAULT_PASSWORD = "Test@12345";
	
	public static final String LOGIN_LINK = "zh-login";
	public static final By LOGIN_LINK_LOCATOR = By.className(LOGIN_LINK);
	
	public static final String LOGIN_ID = "login_id";
	public static final By LOGIN_ID_LOCATOR = By.id(LOGIN_ID);
	
	public static final String NEXT_BTN = "//*[ @id='nextbtn' and @class='btn blue']";
	public static final By NEXT_BTN_LOCATOR = By.xpath(NEXT_BTN);
	
	
	public static final String PWD_ID = "password";
	public static final By PWD_ID_LOCATOR = By.id(PWD_ID);
	
	// Page Title
	public static final String HOME_PAGE_TITLE = "Zoho - Cloud Software Suite and SaaS Applications for Businesses";
	
	public static final String HOME_PAGE_TEXT = "//*[@id='block-system-main']/div[1]/div/h1";
	public static final By HOME_PAGE_TEXT_LOCATOR = By.xpath(HOME_PAGE_TEXT);
	
	// Report Path
	public static final String REPORTS_PATH = System.getProperty("user.dir")+"//Reports//";
	
	public static final String DIV_FIRST_COL = "div.zl-nth-child1";
	public static final By DIV_FIRST_COL_LOCATOR = By.cssSelector(DIV_FIRST_COL);
	public static final String DIV_SECOND_COL = "div.zl-nth-child2";
	public static final By DIV_SECOND_COL_LOCATOR = By.cssSelector(DIV_SECOND_COL);
	public static final String DIV_THIRD_COL = "div.zl-nth-child3";
	public static final By DIV_THIRD_COL_LOCATOR = By.cssSelector(DIV_THIRD_COL);
	public static final String DIV_FOURTH_COL = "div.zl-nth-child4";
	public static final By DIV_FOUTH_COL_LOCATOR = By.cssSelector(DIV_FOURTH_COL);
	//public static final String INTERMEDIATEPAGE_OPTIONS_LINK = "//div[text()='%S']";
	public static final String CRM_LINK = "//div[text()='CRM']";
	public static final String[] intermediatePageOptions = {"Books","Connect","Mail","Writer","Calendar","CRM","Sheet","Campaigns","Desk","Show","Cliq","Invoice","WorkDrive"};
	
	public static final String CONTACTS_HEADER = "//div[@data-value='Contacts']";
	public static final By CONTACTS_HEADER_LOCATOR = By.xpath(CONTACTS_HEADER);
	
	public static final String LEADS_HEADER = "//div[@data-value='Leads']";
	public static final By LEADS_HEADER_LOCATOR = By.xpath(LEADS_HEADER);
	
	
	public static final String ADD_ICON = "createIcon";
	public static final By ADD_ICON_LOCATOR = By.id(ADD_ICON);


	public static final String SUBMENU_LEADS = "submenu_Leads";
	public static final By SUBMENU_LEADS_LOCATOR = By.id(SUBMENU_LEADS);

	public static final String SUBMENU_CONTACTS = "submenu_Contacts";
	public static final By SUBMENU_CONTACTS_LOCATOR = By.id(SUBMENU_CONTACTS);

	public static final String SUBMENU_TASK = "submenu_Tasks";
	public static final By SUBMENU_TASK_LOCATOR = By.id(SUBMENU_TASK);
					
	public static final String LEAD_COMPANY = "Crm_Leads_COMPANY";
	public static final By LEAD_COMPANY_LOCATOR = By.id(LEAD_COMPANY);		

	public static final String LEAD_LASTNAME = "Crm_Leads_LASTNAME";
	public static final By LEAD_LASTNAME_LOCATOR = By.id(LEAD_LASTNAME);

	public static final String LEAD_SAVE = "saveLeadsBtn";
	public static final By LEAD_SAVE_LOCATOR = By.id(LEAD_SAVE);

	
	public static final String LEAD_DETAILS_BACKBTN = "backArrowDV";
	public static final By LEAD_DETAILS_BACKBTN_LOCATOR = By.id(LEAD_DETAILS_BACKBTN);
	
	public static final String LEAD_NAME_DISPLAYED = "//*[@id='subvalue_LASTNAME' and @class='bCardHover']";
	public static final By LEAD_NAME_DISPLAYED_LOCATOR = By.xpath(LEAD_NAME_DISPLAYED);

	public static final String LEAD_COMPANY_DISPLAYED = "//*[@id='subvalue_COMPANY' and @class='bCardHover']";
	public static final By LEAD_COMPANY_DISPLAYED_LOCATOR = By.xpath(LEAD_COMPANY_DISPLAYED);
	
	public static final String TASK_SUBJECT = "Crm_Tasks_SUBJECT";
	public static final By TASK_SUBJECT_LOCATOR = By.id(TASK_SUBJECT);

	public static final String TASK_DUEDATE_CALENDAR = "Crm_Tasks_DUEDATE";
	public static final By TASK_DUEDATE_CALENDAR_LOCATOR = By.id(TASK_DUEDATE_CALENDAR);
	
	public static final String DATE_MONTH_YEAR = "//*[@class='sCalMon']";
	public static final By DATE_MONTH_YEAR_LOCATOR = By.xpath(DATE_MONTH_YEAR);

	public static final String DATE_MONTH_FORWARD_BTN = "//*[@class='calNav dRgt']";
	public static final By DATE_MONTH_FORWARD_BTN_LOCATOR = By.xpath(DATE_MONTH_FORWARD_BTN);
	
	public static final String DATE_MONTH_BACKWARD_BTN = "//*[@class='calNav dLft']";
	public static final By DATE_MONTH_BACKWARD_BTN_LOCATOR = By.xpath(DATE_MONTH_BACKWARD_BTN);
	
	//Column headers of excel
	public static final String COL_DATE = "Date";
	public static final String COL_SUBJECT = "Subject";
	public static final String COL_RUNMODE = "Runmode";
	public static final String RUNMODE_YES = "Y";
	public static final String RUNMODE_NO = "N";
	public static final String COL_LEAD_COMPANY = "LeadCompany";
	public static final String COL_LEAD_NAME = "LeadName";
	public static final String COL_USERNAME = "Username";
	public static final String COL_PASSWORD = "Password";
	public static final String COL_USERNAMEVALID = "UsernameValid";
	public static final String COL_TCID = "TCID";
	
	// sheet names
	
	public static final String TEST_STATUS_SHEET = "Test Status";
	public static final String TEST_CASES_SHEET = "TestCases";	
	
}
