package com.Zoho.pages.base;

import java.util.Hashtable;

import com.Zoho.pages.session.lead.LeadsDetailsPage;
import com.Zoho.pages.session.lead.LeadsPage;

public interface ZohoSessionPage {

	// Session based Operations
	void createDeal();
	void logout();
	public ZohoPage validateIntermediatePageOptions();
	public ZohoPage gotoDefaultLandingPage();
	public LeadsDetailsPage submitLeadDetails(String strLeadCompany, String strLeadLastName);
	public LeadsPage gotoLeadsPage();
	public ZohoPage validateLeadDetails();
	public ZohoPage validateLeadPresenceInList();
	public ZohoPage createTask (Hashtable <String,String> data);
}
