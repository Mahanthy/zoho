package com.Zoho.pages.normal;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoBasePage;
import com.Zoho.pages.base.ZohoPage;

public class EnterUserNamePage extends ZohoBasePage{

	@FindBy(id=Constant.LOGIN_ID)
	WebElement loginUserName;
	
	@FindBy(xpath=Constant.NEXT_BTN)
	WebElement nextButton;
	
	
	public ZohoPage submitUser(String strUserName) {
		waitForPageToLoad();
		loginUserName.sendKeys(strUserName);
		// explicit wait
		nextButton.click();
		boolean presencePasswordField = validator(false).isElementPresent(Constant.PWD_ID_LOCATOR);
		if (!presencePasswordField)
			return this;
		else
			return new EnterPasswordPage();
	}

}
