package com.Zoho.pages.normal;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoBasePage;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.session.DefaultLandingPage;
import com.Zoho.pages.session.IntermediatePage;

public class EnterPasswordPage extends ZohoBasePage{
	@FindBy(id=Constant.PWD_ID)
	WebElement loginPassword;
	
	@FindBy(xpath=Constant.NEXT_BTN)
	WebElement nextButton;
	
	public ZohoPage submitPassword(String strPassword) {
		loginPassword.sendKeys(strPassword);
		nextButton.click();
		return new IntermediatePage();
	}

	
}
