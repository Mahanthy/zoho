package com.Zoho.pages.normal;

import org.openqa.selenium.support.PageFactory;

import com.Zoho.pages.base.ZohoBasePage;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.session.ZohoTestSession;

public class LaunchPage extends ZohoBasePage {
	
	public LaunchPage() {
		System.out.println ("**** Launch Page Constructor ****");
		
	}
	
	public ZohoPage openBrowser(String browser) {
		// TODO Auto-generated method stub
		getDriver().openBrowser(browser);
		
		return this;
	
	}

	public ZohoPage navigatetoHomePage() {
		log( "Navigating to https://www.zoho.com Page");
		getDriver().navigate("https://www.zoho.com");
		return new HomePage();
	}

}
