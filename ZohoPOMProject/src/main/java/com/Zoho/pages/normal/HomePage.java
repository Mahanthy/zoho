package com.Zoho.pages.normal;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoBasePage;
import com.Zoho.pages.base.ZohoPage;


public class HomePage extends ZohoBasePage{
	@FindBy(className=Constant.LOGIN_LINK)
	WebElement loginLink;
	
	public HomePage() {
		System.out.println ("**** Home Page Constructor ****");
	}
	public ZohoPage gotoEnterUserNamePage() {
		log( "Going to UserName Page");
		loginLink.click();
		return new EnterUserNamePage();
	}

}
