package com.Zoho.pages.session;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoBasePage;
import com.Zoho.pages.base.ZohoPage;

public class IntermediatePage extends ZohoBaseSessionPage {
	@FindAll({
		@FindBy(css=Constant.DIV_FIRST_COL),
		@FindBy(css=Constant.DIV_SECOND_COL),
		@FindBy(css=Constant.DIV_THIRD_COL),
		@FindBy(css=Constant.DIV_FOURTH_COL),
	})
	public List <WebElement> pageOptions;
	//String x = String.format(Constant.INTERMEDIATEPAGE_OPTIONS_LINK,"CRM");
	@FindBy(xpath=Constant.CRM_LINK)
	public WebElement crm_link;
	
	public ZohoPage validateIntermediatePageOptions() {
		System.out.println(pageOptions.size());
		List <String> intermediatePageOptions = Arrays.asList(Constant.intermediatePageOptions);
		for (WebElement e:pageOptions) {
			System.out.println(e.getText()+ "::::" + intermediatePageOptions.contains(e.getText()));
			if (!intermediatePageOptions.contains(e.getText()))
				validator(false).fail("Option not found. " + e.getText());
		}
		return this;
	}
	
	public ZohoPage gotoDefaultLandingPage() {
		crm_link.click();
		return new DefaultLandingPage();
	}
}
