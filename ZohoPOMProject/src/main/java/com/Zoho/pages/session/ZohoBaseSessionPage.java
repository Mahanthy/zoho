package com.Zoho.pages.session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoBasePage;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.components.common.session.Header;

public class ZohoBaseSessionPage extends ZohoBasePage {

	Header header;
	@FindBy(xpath=Constant.DATE_MONTH_YEAR)
	public WebElement dateMonth;
	@FindBy(xpath=Constant.DATE_MONTH_FORWARD_BTN)
	public WebElement forwardBtn;
	@FindBy(xpath=Constant.DATE_MONTH_BACKWARD_BTN)
	public WebElement backwardBtn;
	
	public ZohoBaseSessionPage() {
		
		header = new Header(getCurrentDriver());
		
	}
	
	public Header getHeader() {
		return header;
	}
	
	public void selectDateFromCalendar(String date) {
		
		try {
			Date currentDate = new Date();
			// string to date format
			Date dateToBeSelected = new SimpleDateFormat("dd-MM-yyyy").parse(date);
			String day = new SimpleDateFormat("d").format(dateToBeSelected);
			String month = new SimpleDateFormat("MMMM").format(dateToBeSelected);
			String year = new SimpleDateFormat("yyyy").format(dateToBeSelected);
			String monthYear = month + " " + year;
			while (!dateMonth.getText().equals(monthYear)) {
				System.out.println(dateMonth.getText() + " ::: " + monthYear);
				if (currentDate.compareTo(dateToBeSelected) > 0) {
					System.out.println(currentDate + " ::: " + dateToBeSelected);
					backwardBtn.click();
				}
				else if (currentDate.compareTo(dateToBeSelected) < 0) {
					forwardBtn.click();
				}
			}
			getCurrentDriver().findElement(By.xpath("//td[text()='"+day+"']")).click();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
