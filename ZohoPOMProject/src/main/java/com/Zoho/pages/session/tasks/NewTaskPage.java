package com.Zoho.pages.session.tasks;

import java.util.Hashtable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.session.ZohoBaseSessionPage;

public class NewTaskPage extends ZohoBaseSessionPage {
	
	@FindBy(id=Constant.TASK_SUBJECT)
	public WebElement subject;

	@FindBy(id=Constant.TASK_DUEDATE_CALENDAR)
	public WebElement duedate;
	
	public ZohoPage createTask (Hashtable <String,String> data) {
		subject.sendKeys(data.get(Constant.COL_SUBJECT));
		duedate.click();
		selectDateFromCalendar(data.get(Constant.COL_DATE));
		// write a method to select contact
		//selectContact(data.get("Contact"));
		return new TaskPage();
	}

}

