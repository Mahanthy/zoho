package com.Zoho.pages.session.lead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.session.ZohoBaseSessionPage;

public class NewLeadsPage extends ZohoBaseSessionPage {

@FindBy(id=Constant.LEAD_COMPANY)
public WebElement company;

@FindBy(id=Constant.LEAD_LASTNAME)
public WebElement lastname;

@FindBy(id=Constant.LEAD_SAVE)
public WebElement saveLeadBtn;


public LeadsDetailsPage submitLeadDetails(String strLeadCompany, String strLeadLastName) {
	
	company.sendKeys(strLeadCompany);
	lastname.sendKeys(strLeadLastName);
	saveLeadBtn.click();
	return new LeadsDetailsPage();
	}

public ZohoPage validateLeadDetails() {
	 return this;
	}
}
