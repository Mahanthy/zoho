package com.Zoho.pages.session.lead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.session.ZohoBaseSessionPage;

public class LeadsDetailsPage extends ZohoBaseSessionPage{

	@FindBy(id=Constant.LEAD_DETAILS_BACKBTN)
	public WebElement leadBackBtn;
	
	public LeadsPage gotoLeadsPage() {
		leadBackBtn.click();
		return new LeadsPage();
	}
}
