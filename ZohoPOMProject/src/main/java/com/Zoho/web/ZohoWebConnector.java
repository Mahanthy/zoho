package com.Zoho.web;

import org.openqa.selenium.By;
import org.testng.asserts.SoftAssert;

import com.Zoho.pages.base.ZohoPage;

public interface ZohoWebConnector {

	 // Application Specific Methods
	
	void logout();
	ZohoPage validateTitle(String strExpectedTitle);
	ZohoPage validateText(By strLocator, String strExpectedText);
	ZohoPage validateElementPresent(By strLocatorType);
	ZohoPage validateElementNotPresent(By strLocatorType);
	void validateLogin();
	// Generic Methods
	void openBrowser(String browsername);
	void navigate(String url);
	public SoftAssert getSoftAssert();
	public void setSoftAssert(SoftAssert softAssert);
	public boolean isElementPresent(By strLocator);
	
}
