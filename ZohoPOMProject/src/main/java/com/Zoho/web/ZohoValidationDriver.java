package com.Zoho.web;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.session.ZohoTestSession;

public abstract class ZohoValidationDriver implements WebConnector {
	EventFiringWebDriver driver;
	boolean stopExecution;
	SoftAssert softAssert = new SoftAssert();
	
	public SoftAssert getSoftAssert() {
		return softAssert;
	}

	public void setSoftAssert(SoftAssert softAssert) {
		this.softAssert = softAssert;
	}

	public boolean isStopExecution() {
		return stopExecution;
	}

	public void setStopExecution(boolean stopExecution) {
		this.stopExecution = stopExecution;
	}

	public ZohoPage validateElementPresent(By strLocatorType)
	{
		// Validation
		if (!isElementPresent(strLocatorType)) {
			fail("Element not found :: " + strLocatorType);
		}
		return getSession().getCurrentPage();
	}
	
	public ZohoPage validateElementNotPresent(By strLocatorType)
	{
		// Validation
		if (isElementPresent(strLocatorType)) {
			fail("Element found :: " + strLocatorType);
		}
		return getSession().getCurrentPage();
	}
	
	
	public ZohoPage validateTitle(String strExpectedTitle) {
     log("Expected Title :::" + strExpectedTitle);	
     log("Actual Title :::" + driver.getTitle());
	 getSession().getCon().setStopExecution(stopExecution);	
	 System.out.println ("Status of isStopExecution::: " + isStopExecution());
	 if (!strExpectedTitle.equals(driver.getTitle()))
	 {

		 
		 fail("The Expected Title Doesnot match. Got Title As ::" + driver.getTitle());
			 
	 }
	 return getSession().getCurrentPage();
	
	
	}

	public void validateLogin() {
		// TODO Auto-generated method stub
		
	}
	
	public ZohoPage validateText(By strLocator, String strExpectedText) {
		// assertion
		 try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     log("Expected Text :::" + strExpectedText);	

		getSession().getCon().setStopExecution(stopExecution);	
		 System.out.println ("Status ::: " + isStopExecution());
		 System.out.println (" ***** ::: " + strLocator);
		 String strActualText = driver.findElement(strLocator).getText();
	     log("Actual Text :::" + strActualText);
		 if (!strExpectedText.equals(strActualText))
		 {
			 fail("Text not matching. Got txt as ::" + strActualText);	 
		 }
		 
		return getSession().getCurrentPage();
	}
	
	public ZohoTestSession getSession() {
		// TODO Auto-generated method stub
		return (ZohoTestSession) Reporter.getCurrentTestResult().getTestContext().getAttribute("session");

	}
	
	public void assertAll() {
		// Add Screenshot
		
		softAssert.assertAll();// Hard Stop. stops execution if there are any errors before
	}
	public void log(String strMessage) {
		getSession().log(strMessage);
	}
	
	public void fail(String strFailureMessage) {
		// Fails in Extent report
		getSession().failTest(strFailureMessage);
		// Fail in TestNG
		softAssert.fail(strFailureMessage);
		// decide if the execution has to be stopped or not.
		if (isStopExecution()) {
			 System.out.println ("Status of softAssert::: " + isStopExecution());
			 assertAll(); 
		 }
	}
	
	public boolean isElementPresent(By strLocator) {
		getSession().setExecuteListner(false);
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(strLocator));
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(strLocator));
			wait.until(ExpectedConditions.elementToBeClickable(strLocator));
		}
		catch(TimeoutException ex){
			return false;
		}
		getSession().setExecuteListner(true);
		return true;
	}
}
