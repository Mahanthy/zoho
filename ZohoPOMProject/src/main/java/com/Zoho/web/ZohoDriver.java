package com.Zoho.web;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.Zoho.listner.ZohoEventListner;
import com.Zoho.session.ZohoTestSession;

public class ZohoDriver extends ZohoValidationDriver {
	
	public void logout() {
		// TODO Auto-generated method stub
		
	}



	public void openBrowser(String browsername) {
		// 
		log("Opening the Browser.."+ browsername);
		System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
		System.setProperty("webdriver.chrome.driver",
				"/Users/apple/eclipse-workspace/ZohoPOMProject/binaries/chromedriver");  
		
	  driver = new EventFiringWebDriver(new ChromeDriver());
	  driver.register(new ZohoEventListner());
	  
	  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  log("Browser Opened Successfully");
	}

	public void navigate(String url) {
		// TODO Auto-generated method stub
		log("URL is .."+ url);
		driver.navigate().to(url);
	}



	public EventFiringWebDriver getCurrentDriver() {
		return driver;
	}



	public void waitForElementLoad() {
		// TODO Auto-generated method stub
		
	}

}
