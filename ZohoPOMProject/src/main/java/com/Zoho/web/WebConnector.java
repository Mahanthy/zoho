package com.Zoho.web;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.asserts.SoftAssert;

import com.Zoho.pages.base.ZohoPage;
import com.Zoho.session.ZohoTestSession;

public interface WebConnector extends ZohoWebConnector{
	
	void openBrowser(String browsername);
	void navigate(String url);
	ZohoPage validateTitle(String strExpectedTitle);
	ZohoPage validateText(By strLocator, String strExpectedText);
	ZohoPage validateElementPresent(By strLocatorType);
	ZohoPage validateElementNotPresent(By strLocatorType);
	EventFiringWebDriver getCurrentDriver();
	void waitForElementLoad();
	ZohoTestSession getSession();
	public boolean isStopExecution();
	public void setStopExecution(boolean stopExecution); 
	public void assertAll();
	public SoftAssert getSoftAssert();
	public void setSoftAssert(SoftAssert softAssert);
	public void log(String strMessage);
	public void fail(String strFailureMessage);
	public boolean isElementPresent(By strLocator);
}
