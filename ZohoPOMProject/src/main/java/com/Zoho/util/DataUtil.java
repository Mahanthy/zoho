package com.Zoho.util;

import java.util.Hashtable;

import com.Zoho.pages.base.Constant;

public class DataUtil {

	public static Object[][] getData(String strTestName, Xls_Reader xls) {
		System.out.println("Start2");
		strTestName = strTestName.toLowerCase();
		String sheetName = "TestCases";
		
		int testStartRowNum = 1;
		System.out.println(xls.getCellData(sheetName, 0, testStartRowNum));
		System.out.println(strTestName);
		while (!xls.getCellData(sheetName, 0, testStartRowNum).equalsIgnoreCase(strTestName)) {
			testStartRowNum++;
		}
		System.out.println("Row number for " + strTestName + " ::: is :" + testStartRowNum);
		
		int colStartRowNum = testStartRowNum + 1;
		
		// Find total number of columns of the test case
		
		int totalCols = 0;
		
		while (!xls.getCellData(sheetName, totalCols, colStartRowNum).equalsIgnoreCase("")) {
			totalCols++;
		}
		
		System.out.println("Total number of Columns for " + strTestName + " ::: is :" + totalCols);
		
		// where data starts for the test case
		
		int dataStartRowNo = colStartRowNum + 1;
		
		// Find total number of rows for a test case
		
		int totalRows = 0;
		
		while (!xls.getCellData(sheetName, 0, dataStartRowNo+totalRows).equals("")) {
			totalRows++;
		}
		
		System.out.println("Total number of rows for " + strTestName + " ::: is :" + totalRows);
		
		Object testData[][] = new Object[totalRows][1];
		Hashtable <String, String> table = null;
		int i = 0; // this is for rowno of the 2D array
		for (int rNum = dataStartRowNo;rNum < dataStartRowNo+totalRows; rNum++) {
			table = new Hashtable<String, String>();
			for(int cNum = 0; cNum <totalCols; cNum++) {
				String key = xls.getCellData(sheetName, cNum, colStartRowNum);
				String data = xls.getCellData(sheetName, cNum, rNum);
				System.out.println (key + " ::: " + data);
				table.put(key, data);
			}
			// copying table data into 2 dim array
			testData[i][0] = table;
			i++;
			
		}
		return testData;
  
		
	}
	
	public static boolean isRunnable(String testName, Xls_Reader xls) {
		testName = testName.toLowerCase();
		String sheetName = Constant.TEST_STATUS_SHEET;
		
		int rows = xls.getRowCount(sheetName);
		for (int rowno=2;rowno <= rows; rowno++ ) {
			
			String testcaseName = xls.getCellData(sheetName, Constant.COL_TCID, rowno);
			String runMode = xls.getCellData(sheetName, Constant.COL_RUNMODE, rowno);
			System.out.println (testcaseName + "::::" + runMode);
			if(testName.equalsIgnoreCase(testcaseName)) {
				if (runMode.equals(Constant.RUNMODE_YES)) {
					return true;
				}
			}
		}
		return false;
	}
}
