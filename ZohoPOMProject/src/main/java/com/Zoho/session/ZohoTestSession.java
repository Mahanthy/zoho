package com.Zoho.session;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Reporter;

import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.normal.LaunchPage;
import com.Zoho.reports.ExtentManager;
import com.Zoho.web.WebConnector;
import com.Zoho.web.ZohoDriver;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class ZohoTestSession {
	WebConnector con;
	ZohoPage currentPage;
	ExtentReports report;
	ExtentTest test;
	boolean executeListner = true;
	
	public boolean isExecuteListner() {
		return executeListner;
	}

	public void setExecuteListner(boolean executeListner) {
		this.executeListner = executeListner;
	}

	public ZohoPage getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(ZohoPage currentPage) {
		this.currentPage = currentPage;
	}

	public ZohoTestSession () {
		con = new ZohoDriver();
	}
	
	public void init(String testName) {
		
		if (Reporter.getCurrentTestResult().getTestContext().getAttribute("session")==null) {
			Reporter.getCurrentTestResult().getTestContext().setAttribute("session",this);

		}
		// Initialize report
		report = ExtentManager.getReport(Constant.REPORTS_PATH);
		test = report.createTest(testName);
		//ZohoPage page = new LaunchPage();
		//return page;
	
	}
	public WebConnector getCon() {
		return con;
	}

	public void end() {
		// TODO Auto-generated method stub
		getCon().assertAll();
	}
	
	/**************** Reporting Functions **************/
	
	public void log(String strMessage) {
		test.log(Status.INFO, strMessage);
	}
	
	public void failTest(String strFailureMessage) {
		// Fail in Extent Report
		test.log(Status.FAIL, strFailureMessage);
		System.out.println("Failing ::: " + strFailureMessage);
		// Take screenshot
		takescreenshot();
		
	}
	
	public void takescreenshot() {
		// TODO Auto-generated method stub
		Date d = new Date();
		String screenshotFile = d.toString().replace(":", "_").replace(" ", "_")+".png";
		File sourceFile = ((TakesScreenshot) getCon().getCurrentDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(sourceFile, new File(ExtentManager.screenshotFolderPath+screenshotFile));
			test.log(Status.INFO, "Screenshot -- > "+ test.addScreenCaptureFromPath(ExtentManager.screenshotFolderPath+screenshotFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateReport() {
		if (report!= null)
			report.flush();
	}
	
	public void skipTest(String strMessage) {
		test.log(Status.SKIP, strMessage);
	}
}
