package com.Zoho.listner;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.Zoho.session.ZohoTestSession;
import com.Zoho.web.WebConnector;

public class ZohoEventListner extends AbstractWebDriverEventListener {


	public void beforeFindBy(By strLocator, WebElement element, WebDriver driver) {
		if (getSession().isExecuteListner()) {
			WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(strLocator));
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(strLocator));
				wait.until(ExpectedConditions.elementToBeClickable(strLocator));
			}
			catch(TimeoutException ex){
				getDriver().fail("Element is not found" + strLocator);
				getDriver().assertAll();
			}
		}
		
	}
	public ZohoTestSession getSession() {
		// TODO Auto-generated method stub
		return (ZohoTestSession) Reporter.getCurrentTestResult().getTestContext().getAttribute("session");
		
	}
	
	public WebConnector getDriver() {
		return getSession().getCon();
	}
	
	public void log(String strMessage) {
		getSession().log(strMessage);
	}

}
