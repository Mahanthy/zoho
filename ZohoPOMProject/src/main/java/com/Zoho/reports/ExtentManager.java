package com.Zoho.reports;

import java.io.File;
import java.util.Date;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentManager {
	static ExtentReports reports;
	public static String screenshotFolderPath;
	public static ExtentReports getReport(String basePath) 
	{
		if (reports == null) {
			Date d = new Date();
			String reportFolder = d.toString().replaceAll(":", "_");
			String path = basePath+reportFolder;
			screenshotFolderPath = path+"/screenshots/";
			File file = new File(screenshotFolderPath);
			file.mkdirs();
			ExtentSparkReporter sparkReporter = new ExtentSparkReporter(path);
			sparkReporter.config().setReportName("Test Report"+reportFolder);
			sparkReporter.config().setDocumentTitle("Automation Report");
			sparkReporter.config().setTheme(Theme.DARK);
			sparkReporter.config().setEncoding("utf-8");
			reports = new ExtentReports();
			reports.attachReporter(sparkReporter);
		}
		
		return reports;
	}
}
