package com.Zoho.suite.testcases.login;

import org.testng.annotations.Test;
import org.testng.Assert;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.Zoho.DataProvider.TestDataProvider;
import com.Zoho.base.TestBase;
import com.Zoho.listner.ZohoEventListner;
import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.normal.EnterPasswordPage;
import com.Zoho.pages.normal.EnterUserNamePage;
import com.Zoho.pages.normal.LaunchPage;
import com.Zoho.reports.ExtentManager;
import com.Zoho.session.ZohoTestSession;
import com.Zoho.util.DataUtil;
import com.Zoho.util.Xls_Reader;
import com.Zoho.web.ZohoDriver;
import com.Zoho.web.ZohoWebConnector;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


public class LoginTest extends TestBase{
	
	
	@Test (dataProviderClass = TestDataProvider.class, dataProvider="getData")
  public void loginTest(Hashtable <String, String> data) {
	  session.log(data.toString());
	  if (!DataUtil.isRunnable(testName, xls) || data.get(Constant.COL_RUNMODE).equalsIgnoreCase(Constant.RUNMODE_NO)) {
		  // Skip in Extent report
		  session.skipTest("Skipping the test as the runmode is set to N");
		  
		  // Skip in TestNG Report
		  throw new SkipException("Skipping the test as the runmode is set to N");
	  }
	  String username = data.get(Constant.COL_USERNAME);
	  String password = data.get(Constant.COL_PASSWORD);
	  String usernameValid = data.get(Constant.COL_USERNAMEVALID);
	  // one driver object
	  // one report object
	  // one Zoho session object
	  
	  //test.log(Status.INFO, "Starting Session Created");
	   ZohoPage page = new LaunchPage()
		   .openBrowser("chrome")
		   .navigatetoHomePage()
		   // validator(false) - the test step will not stop, true means it will stop.
		   .validator(false).validateTitle(Constant.HOME_PAGE_TITLE)
		   .validator(false).validateText(Constant.HOME_PAGE_TEXT_LOCATOR, "Your Life's Work, Powered By Our Life's Work")
		   //.validator(true).validateElementPresent(Constant.LOGIN_LINK_LOCATOR)
		   
		   .gotoEnterUserNamePage()
		   .submitUser(username);
	   	if (page instanceof EnterUserNamePage && usernameValid.equals("Y"))  {
		   //failure
	   		Assert.fail("Entered valid user name but could not login");
	   	}
	   	else if (page instanceof EnterPasswordPage) {
	   		if (usernameValid.equals("N")) {
	   			// failure
	   			Assert.fail("Able to enter submit with valid username" + usernameValid);
	   		}
	   		else {
	   			// proceed with password entry and logged in
	   			page.submitPassword(password);
	   		}
	   	}

	   session.end(); // should always be inside the test case.

	   //test.log(Status.PASS, "LOGIN TEST PASSED");
	   	
  }

}
