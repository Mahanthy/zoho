package com.Zoho.suite.testcases.create;

import org.testng.annotations.Test;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.Zoho.DataProvider.TestDataProvider;
import com.Zoho.base.TestBase;
import com.Zoho.listner.ZohoEventListner;
import com.Zoho.pages.base.Constant;
import com.Zoho.pages.base.ZohoPage;
import com.Zoho.pages.normal.EnterPasswordPage;
import com.Zoho.pages.normal.EnterUserNamePage;
import com.Zoho.pages.normal.LaunchPage;
import com.Zoho.reports.ExtentManager;
import com.Zoho.session.ZohoTestSession;
import com.Zoho.util.DataUtil;
import com.Zoho.util.Xls_Reader;
import com.Zoho.web.ZohoDriver;
import com.Zoho.web.ZohoWebConnector;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


public class CreateTaskTest extends TestBase{

	
  @Test (dataProviderClass = TestDataProvider.class, dataProvider="getData")
  public void createTaskTest(Hashtable <String, String> data) {
	  session.log(data.toString());
	  if (!DataUtil.isRunnable(testName, xls) || data.get(Constant.COL_RUNMODE).equalsIgnoreCase(Constant.RUNMODE_NO)) {
		  // Skip in Extent report
		  session.skipTest("Skipping the test as the runmode is set to N");
		  
		  // Skip in TestNG Report
		  throw new SkipException("Skipping the test as the runmode is set to N");
	  }

	  // one driver object
	  // one report object
	  // one Zoho session object
	  
	  //test.log(Status.INFO, "Starting Session Created");
	   ZohoPage page = new LaunchPage()
		   .openBrowser("chrome")
		   .navigatetoHomePage()
		   // validator(false) - the test step will not stop, true means it will stop.
		   .validator(false).validateTitle(Constant.HOME_PAGE_TITLE)
		   .validator(false).validateText(Constant.HOME_PAGE_TEXT_LOCATOR, "Your Life's Work, Powered By Our Life's Work")
		   
		   .gotoEnterUserNamePage()
		   .submitUser(Constant.DEFAULT_USERNAME)
		   .submitPassword(Constant.DEFAULT_PASSWORD)
		   .validateIntermediatePageOptions()
		   .gotoDefaultLandingPage()
		   .getHeader().create("Task")
		   .createTask(data);
	       
	   
	   session.end(); // should always be inside the test case.

	   //test.log(Status.PASS, "LOGIN TEST PASSED");
	   	
  }

  
  @DataProvider
  public Object[][] getData() {
	  return DataUtil.getData(testName, xls);
  }
}
