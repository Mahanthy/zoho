package com.Zoho.DataProvider;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;

import com.Zoho.util.DataUtil;
import com.Zoho.util.Xls_Reader;

public class TestDataProvider {
	static Xls_Reader xls = new Xls_Reader(System.getProperty("user.dir")+"/Data.xlsx");

	  @DataProvider
	  public static Object[][] getData(Method m) {
		  System.out.println ("**** Data Provider ***");
		  return DataUtil.getData(m.getName(), xls);
	  }

}
