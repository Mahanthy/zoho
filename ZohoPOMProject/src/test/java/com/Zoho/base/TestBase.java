package com.Zoho.base;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.Zoho.session.ZohoTestSession;
import com.Zoho.util.Xls_Reader;

public class TestBase {
	

	public ZohoTestSession session;
	public String testName = null;
	public Xls_Reader xls = new Xls_Reader(System.getProperty("user.dir")+"/Data.xlsx");
	@BeforeMethod
	public void start(ITestResult result) {
		  System.out.println ("**** Test Base Before Method***");
		testName = result.getMethod().getMethodName().toUpperCase();
		session = new ZohoTestSession();
		session.init(testName);
			   
	}
	
	  @AfterMethod
	  public void quit() {
		  //report.flush(); // this will create a HTML File/report
		 session.generateReport();
	  }

}
